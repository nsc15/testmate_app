package com.testmate.app.TestMate.language;

import com.testmate.app.TestMate.dto.LanguageDto;
import com.testmate.app.TestMate.entity.LanguageEntity;
import com.testmate.app.TestMate.repository.ILanguageRepository;
import com.testmate.app.TestMate.service.LanguageServiceImpl;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class LanguageServiceImplTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ILanguageRepository languageRepository;

    @MockBean
    private ModelMapper modelMapper;

    @InjectMocks
    private LanguageServiceImpl languageService;

    @Test
    @Order(1)
    public void testFetchAllLanguages() throws Exception {
        when(languageRepository.findAll()).thenReturn(Arrays.asList(new LanguageEntity("Java"), new LanguageEntity("Python")));

        mockMvc.perform(get("/language"))
                .andExpect(status().isOk());
    }

    //    @Test
//    public void testGetLanguageById_Success() throws Exception {
//        // Arrange
//        String languageId = "1";
//        LanguageEntity mockEntity = new LanguageEntity("1", "Java");
//        LanguageDto expectedDto = new LanguageDto("1", "Java");
//        Mockito.when(languageRepository.findById(languageId)).thenReturn(Optional.of(mockEntity));
//        Mockito.when(modelMapper.map(mockEntity, LanguageDto.class)).thenReturn(expectedDto);
//
//        // Act
//        LanguageEntity resultEntity = languageService.getLanguageById(languageId);
//
//        // Assert
//        assertEquals(expectedDto, resultEntity);
//
//    }
    @Test
    @Order(2)
    public void testGetLanguageById_Success() throws Exception {
        // Arrange
        String languageId = "1";
        LanguageEntity mockEntity = new LanguageEntity("1", "Java");
        Mockito.when(languageRepository.findById(languageId)).thenReturn(Optional.of(mockEntity));

        // Act
        LanguageEntity resultEntity = languageService.getLanguageById(languageId);

        // Assert
        assertNotNull(resultEntity);
        assertEquals(mockEntity, resultEntity); // Compare LanguageEntity objects
    }

}
