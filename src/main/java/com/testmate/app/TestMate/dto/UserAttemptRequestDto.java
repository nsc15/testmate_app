package com.testmate.app.TestMate.dto;

import com.testmate.app.TestMate.entity.QuestionnaireEntity;
import com.testmate.app.TestMate.entity.UserEntity;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserAttemptRequestDto {
    private String id;
    private UserEntity user;
    private QuestionnaireEntity questionnaire;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private int score;
    private int totalAttempted;
    private int totalUnsolved;
}
