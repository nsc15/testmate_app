package com.testmate.app.TestMate.dto;

import lombok.*;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LanguageDto {

    private String id;

    private String name;
}
