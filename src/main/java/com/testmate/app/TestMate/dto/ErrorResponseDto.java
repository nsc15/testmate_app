package com.testmate.app.TestMate.dto;

import lombok.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponseDto {
    private String message;
}
