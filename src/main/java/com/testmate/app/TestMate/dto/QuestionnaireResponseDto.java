package com.testmate.app.TestMate.dto;

import com.testmate.app.TestMate.entity.ExamEntity;
import com.testmate.app.TestMate.entity.QuestionBankEntity;
import com.testmate.app.TestMate.enums.Difficulty;
import lombok.*;

import java.util.Set;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionnaireResponseDto {
    private String id;
    private ExamEntity exam;
    private int numQuestions;
    private Difficulty minDifficulty;
    private Difficulty maxDifficulty;
    private String tags;
    private Set<QuestionBankEntity> questions;
}
