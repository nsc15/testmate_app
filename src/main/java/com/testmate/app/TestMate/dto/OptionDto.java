package com.testmate.app.TestMate.dto;

import com.testmate.app.TestMate.entity.QuestionBankEntity;
import lombok.*;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OptionDto {
    private String id;
    private String text;
    private QuestionBankEntity question;
}
