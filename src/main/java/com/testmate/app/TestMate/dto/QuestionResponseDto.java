package com.testmate.app.TestMate.dto;

import com.testmate.app.TestMate.entity.LanguageEntity;
import com.testmate.app.TestMate.entity.OptionEntity;
import com.testmate.app.TestMate.enums.Difficulty;
import lombok.*;

import java.util.List;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponseDto {

    private String id;
    private LanguageEntity language;
    private String question;
    private List<OptionEntity> options;
    private Difficulty difficultyLevel;
    private String tags;
}
