package com.testmate.app.TestMate.dto;

import com.testmate.app.TestMate.entity.UserEntity;
import lombok.*;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserAttemptResponseDto {
    private String id;
    private UserEntity user;
    private int score;
    private int totalAttempted;
    private int totalUnsolved;
    private boolean result;
}
