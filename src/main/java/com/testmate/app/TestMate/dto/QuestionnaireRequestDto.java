package com.testmate.app.TestMate.dto;

import com.testmate.app.TestMate.entity.ExamEntity;
import com.testmate.app.TestMate.enums.Difficulty;
import lombok.*;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionnaireRequestDto {
    private ExamEntity exam;
    private int numQuestions;
    private Difficulty minDifficulty;
    private Difficulty maxDifficulty;
    private String tags;
}
