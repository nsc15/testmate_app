package com.testmate.app.TestMate.dto;

import lombok.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExamDto {
    private String id;
    private String name;
    private int durationInMinutes;
    private int passingScore;
}
