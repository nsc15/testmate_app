package com.testmate.app.TestMate.repository;

import com.testmate.app.TestMate.entity.QuestionnaireEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IQuestionnaireRepository extends JpaRepository<QuestionnaireEntity, String> {
}
