package com.testmate.app.TestMate.repository;

import com.testmate.app.TestMate.entity.OptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IOptionRepository extends JpaRepository<OptionEntity, String> {

    public List<OptionEntity> findAllByQuestionId(String questionId);
}
