package com.testmate.app.TestMate.repository;

import com.testmate.app.TestMate.entity.UserAttemptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserAttemptRepository extends JpaRepository<UserAttemptEntity, String> {

    public List<UserAttemptEntity> findByUserId(String userId);

    public List<UserAttemptEntity> findByExamId(String examId);
}
