package com.testmate.app.TestMate.repository;

import com.testmate.app.TestMate.entity.ExamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IExamRepository extends JpaRepository<ExamEntity, String> {
}
