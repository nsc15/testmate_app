package com.testmate.app.TestMate.repository;

import com.testmate.app.TestMate.entity.QuestionBankEntity;
import com.testmate.app.TestMate.enums.Difficulty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface IQuestionBankRepository extends JpaRepository<QuestionBankEntity, String> {
    public List<QuestionBankEntity> findByDifficultyLevel(Difficulty val);
}

