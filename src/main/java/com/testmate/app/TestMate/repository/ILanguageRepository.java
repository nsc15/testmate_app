package com.testmate.app.TestMate.repository;

import com.testmate.app.TestMate.entity.LanguageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILanguageRepository extends JpaRepository<LanguageEntity, String> {
}
