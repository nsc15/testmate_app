package com.testmate.app.TestMate.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    private static final String LANGUAGES_ENDPOINT = "/languages/**";
    private static final String QUESTIONS_ENDPOINT = "/questions/**";
    private static final String EXAMS_ENDPOINT = "/exams/**";
    private static final String QUESTIONNAIRE_ENDPOINT = "/questionnaires/**";
    private static final String USER_ENDPOINT = "/users/**";
    private static final String USER_ATTEMPTS_ENDPOINT = "/attempts/**";
    public static final String[] ENDPOINTS_WHITELIST = {
            LANGUAGES_ENDPOINT,
            QUESTIONS_ENDPOINT,
            EXAMS_ENDPOINT,
            QUESTIONNAIRE_ENDPOINT,
            USER_ENDPOINT,
            USER_ATTEMPTS_ENDPOINT
    };

    // whitelist the endpoints
    @Bean
    public WebSecurityCustomizer configure() {
        return web -> web.ignoring().requestMatchers(ENDPOINTS_WHITELIST);
    }
}
