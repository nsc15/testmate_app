package com.testmate.app.TestMate.controller;

import com.testmate.app.TestMate.dto.UserAttemptRequestDto;
import com.testmate.app.TestMate.dto.UserAttemptResponseDto;
import com.testmate.app.TestMate.entity.UserAttemptEntity;
import com.testmate.app.TestMate.service.IUserAttemptService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/attempts")
public class UserAttemptController {
    @Autowired
    private IUserAttemptService userAttemptService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping("/user/{id}")
    public ResponseEntity<List<UserAttemptResponseDto>> listAllByUserId(@PathVariable("id") String id) {
        var dtoList = userAttemptService.fetchByUserId(id)
                .stream()
                .map(e -> mapper.map(e, UserAttemptResponseDto.class))
                .toList();
        return ResponseEntity.status(HttpStatus.OK)
                .body(dtoList);
    }

    @GetMapping("/exam/{id}")
    public ResponseEntity<List<UserAttemptResponseDto>> listAllByExamId(@PathVariable("id") String id) {
        var dtoList = userAttemptService.fetchByUserId(id)
                .stream()
                .map(e -> mapper.map(e, UserAttemptResponseDto.class))
                .toList();
        return ResponseEntity.status(HttpStatus.OK)
                .body(dtoList);
    }

    @PostMapping()
    public ResponseEntity<UserAttemptResponseDto> add(@RequestBody UserAttemptRequestDto dto) {
        var e = userAttemptService.generate(mapper.map(dto, UserAttemptEntity.class));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.map(e, UserAttemptResponseDto.class));
    }

}
