package com.testmate.app.TestMate.controller;

import com.testmate.app.TestMate.dto.ErrorResponseDto;
import com.testmate.app.TestMate.dto.ExamDto;
import com.testmate.app.TestMate.entity.ExamEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.service.IExamService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/exams")
public class ExamController {
    @Autowired
    private IExamService service;
    @Autowired
    private ModelMapper mapper;

    @GetMapping("/listAll")
    public ResponseEntity<List<ExamDto>> listAll() {
        var dtoList = service.listAll().stream()
                .map(e -> mapper.map(e, ExamDto.class))
                .toList();
        return ResponseEntity.status(HttpStatus.OK)
                .body(dtoList);
    }

    @PostMapping()
    public ResponseEntity<ExamDto> addExam(@RequestBody ExamDto dto) {
        var savedExamDto = service.add(mapper.map(dto, ExamEntity.class));

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.map(savedExamDto, ExamDto.class));
    }

    @PutMapping()
    public ResponseEntity<ExamDto> update(@RequestBody ExamDto dto) {
        var savedExamDto = service.update(mapper.map(dto, ExamEntity.class));

        return ResponseEntity.status(HttpStatus.OK)
                .body(mapper.map(savedExamDto, ExamDto.class));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ExamDto> get(@PathVariable("id") String id) {
        var examEntity = service.get(id);

        return ResponseEntity.status(HttpStatus.OK)
                .body(mapper.map(examEntity, ExamDto.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        String msg = service.delete(id);

        return ResponseEntity.status(HttpStatus.OK)
                .body(msg);
    }

    @ExceptionHandler()
    public ResponseEntity<ErrorResponseDto> handleExceptions(Exception e) {
        e.printStackTrace();
        if (e instanceof NotFoundException)
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResponseDto(e.getMessage()));

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponseDto("Something went wrong."));

    }
}
