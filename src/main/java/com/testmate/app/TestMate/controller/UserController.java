package com.testmate.app.TestMate.controller;

import com.testmate.app.TestMate.dto.ErrorResponseDto;
import com.testmate.app.TestMate.dto.UserRequestDto;
import com.testmate.app.TestMate.dto.UserResponseDto;
import com.testmate.app.TestMate.entity.UserEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.service.IUserService;
import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private IUserService userService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping()
    public ResponseEntity<List<UserResponseDto>> listAllUsers() {
        var dtosList = userService.listAll().stream()
                .map(e -> mapper.map(e, UserResponseDto.class)).toList();
        return ResponseEntity.status(HttpStatus.OK)
                .body(dtosList);
    }


    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto> get(@PathVariable("id") String id) {
        var uEntity = userService.getUser(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(mapper.map(uEntity, UserResponseDto.class));
    }

    @PostMapping("")
    public ResponseEntity<UserResponseDto> add(@RequestBody() UserRequestDto dto) {
        var uEntity = userService.createUser(mapper.map(dto, UserEntity.class));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.map(uEntity, UserResponseDto.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        var msg = userService.delete(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(msg);
    }

    @ExceptionHandler()
    public ResponseEntity<ErrorResponseDto> handleUserExceptions(Exception e) {
        e.printStackTrace();
        if (e instanceof ConstraintViolationException || e instanceof DataIntegrityViolationException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponseDto("User already exists."));
        }

        if (e instanceof NotFoundException)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponseDto(e.getMessage()));

        return ResponseEntity.internalServerError().body(new ErrorResponseDto("Something went wrong."));

    }

}
