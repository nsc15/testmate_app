package com.testmate.app.TestMate.controller;

import com.testmate.app.TestMate.dto.ErrorResponseDto;
import com.testmate.app.TestMate.dto.QuestionResponseDto;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.service.IQuestionBankService;
import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionBankController {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private IQuestionBankService questionBankService;

    @PostMapping("/add")
    public ResponseEntity<QuestionResponseDto> add(@RequestBody QuestionResponseDto dto) {
        var savedDto = questionBankService.add(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(savedDto, QuestionResponseDto.class));
    }

    @PutMapping("/update")
    public ResponseEntity<QuestionResponseDto> update(@RequestBody QuestionResponseDto dto) {
        // TODO: implement service class to update question
        return ResponseEntity.status(HttpStatus.OK).body(new QuestionResponseDto());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        String msg = questionBankService.delete(id);
        return ResponseEntity.ok(msg);
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<QuestionResponseDto>> fetchAllQuestions() {
        var questionsList = questionBankService.listAll().stream()
                .map((entity) -> mapper.map(entity, QuestionResponseDto.class))
                .toList();
        return ResponseEntity.status(HttpStatus.OK).body(questionsList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionResponseDto> get(@PathVariable("id") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(mapper.map(questionBankService.get(id), QuestionResponseDto.class));
    }

    @ExceptionHandler({
            Exception.class
    })
    public ResponseEntity<ErrorResponseDto> handleException(Exception e) {
        e.printStackTrace();
        if (e instanceof ConstraintViolationException || e instanceof DataIntegrityViolationException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponseDto("Question already exists."));
        }

        if (e instanceof NotFoundException)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponseDto(e.getMessage()));

        return ResponseEntity.internalServerError().body(new ErrorResponseDto("Something went wrong."));
    }
}
