package com.testmate.app.TestMate.controller;

import com.testmate.app.TestMate.dto.ErrorResponseDto;
import com.testmate.app.TestMate.dto.QuestionnaireRequestDto;
import com.testmate.app.TestMate.dto.QuestionnaireResponseDto;
import com.testmate.app.TestMate.entity.QuestionnaireEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.service.IQuestionnaireService;
import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/questionnaires")
public class QuestionnaireController {

    @Autowired
    private IQuestionnaireService questionnaireService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping("")
    public ResponseEntity<List<QuestionnaireResponseDto>> fetchAll() {
        var dtosList = questionnaireService.listAll().stream().map(e -> mapper.map(e, QuestionnaireResponseDto.class)).toList();
        return ResponseEntity.status(HttpStatus.OK)
                .body(dtosList);

    }

    @PostMapping("")
    public ResponseEntity<QuestionnaireResponseDto> add(@RequestBody QuestionnaireRequestDto dto) {
        var entity = questionnaireService.add(mapper.map(dto, QuestionnaireEntity.class));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.map(entity, QuestionnaireResponseDto.class));
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionnaireResponseDto> get(@PathVariable("id") String id) {
        var entity = questionnaireService.get(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(mapper.map(entity, QuestionnaireResponseDto.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        var msg = questionnaireService.delete(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(msg);
    }

    @ExceptionHandler()
    public ResponseEntity<ErrorResponseDto> handleUserExceptions(Exception e) {
        e.printStackTrace();

        if (e instanceof NotFoundException)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponseDto(e.getMessage()));

        return ResponseEntity.internalServerError().body(new ErrorResponseDto("Something went wrong."));

    }
}
