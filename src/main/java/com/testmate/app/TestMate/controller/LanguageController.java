package com.testmate.app.TestMate.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import com.testmate.app.TestMate.entity.LanguageEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;

import com.testmate.app.TestMate.dto.ErrorResponseDto;
import com.testmate.app.TestMate.dto.LanguageDto;
import com.testmate.app.TestMate.service.ILanguageService;


@RestController
@RequestMapping("/languages")
public class LanguageController {

    @Autowired
    private ILanguageService languageService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<List<LanguageDto>> getLanguagesList() {
        List<LanguageEntity> languagesList = this.languageService.fetchLanguageList();

        if (languagesList == null) {
            return ResponseEntity.notFound().build();
        }

        // Convert the list of entities to a list of DTOs using ModelMapper
        List<LanguageDto> languageDtoList = languagesList.stream()
                .map(languageEntity -> modelMapper.map(languageEntity, LanguageDto.class))
                .collect(Collectors.toList());
        return ResponseEntity.status(200).body(languageDtoList);
    }

    @PostMapping()
    public ResponseEntity<LanguageDto> createLanguage(@RequestBody LanguageDto dto) {
        var savedLanguage = this.languageService.save(dto);
        return ResponseEntity.status(201).body(modelMapper.map(savedLanguage, LanguageDto.class));
    }

    @GetMapping("/{id}")
    public ResponseEntity<LanguageDto> getLanguageById(@PathVariable("id") String id) {
        var languageEntity = this.languageService.getLanguageById(id);
        return ResponseEntity.ok(modelMapper.map(languageEntity, LanguageDto.class));
    }

    @ExceptionHandler({
            Exception.class
    })
    public ResponseEntity<ErrorResponseDto> handleException(Exception e) {
        e.printStackTrace();
        if (e instanceof ConstraintViolationException || e instanceof DataIntegrityViolationException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponseDto("Language already exists."));
        }

        if (e instanceof NotFoundException)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponseDto(e.getMessage()));

        return ResponseEntity.internalServerError().body(new ErrorResponseDto("Something went wrong."));
    }
}
