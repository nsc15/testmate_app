package com.testmate.app.TestMate.enums;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD,
    VERY_HARD
}
