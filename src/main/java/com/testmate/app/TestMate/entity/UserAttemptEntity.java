package com.testmate.app.TestMate.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_attempts")
public class UserAttemptEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "questionnaire_id")
    private QuestionnaireEntity questionnaire;

    @ManyToOne
    @JoinColumn(name = "exam_id")
    private ExamEntity exam;

    @Column(nullable = false, name = "start_time")
    private LocalDateTime startTime;

    @Column(nullable = false, name = "end_time")
    private LocalDateTime endTime;

    private int score;
    @Column(nullable = false, name = "total_attempted")

    private int totalAttempted;

    @Column(nullable = false, name = "total_unsolved")
    private int totalUnsolved;

    @Column(nullable = true, name = "result",columnDefinition = "boolean default false")
    private boolean result;
}
