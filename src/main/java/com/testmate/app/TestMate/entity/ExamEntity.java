package com.testmate.app.TestMate.entity;

import jakarta.persistence.*;
import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "exams")
public class ExamEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(unique = true, nullable = false)
    private String name;

    @Column(name = "duration_in_minutes", nullable = false)
    private int durationInMinutes;

    @Column(nullable = false)
    private int passingScore;
}
