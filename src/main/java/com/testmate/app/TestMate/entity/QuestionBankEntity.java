package com.testmate.app.TestMate.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.testmate.app.TestMate.enums.Difficulty;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "questions_bank")
public class QuestionBankEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "language_id")
    private LanguageEntity language;

    @Column(unique = true, nullable = false)
    private String question;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<OptionEntity> options;

    @Column(nullable = false)
    private Difficulty difficultyLevel;

    @Column(nullable = false)
    private String tags;
}
