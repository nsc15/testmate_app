package com.testmate.app.TestMate.entity;

import com.testmate.app.TestMate.enums.Difficulty;
import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "questionnaires")
public class QuestionnaireEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(nullable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "exam_id")
    private ExamEntity exam;

    @Column(nullable = false)
    private int numQuestions;

    @Column(nullable = false)
    private Difficulty minDifficulty;  // Optional

    @Column(nullable = false)
    private Difficulty maxDifficulty;  // Optional

    @Column(nullable = true)
    private String tags;

    @ManyToMany
    @JoinTable(
            name = "questionnaire_questions",
            joinColumns = @JoinColumn(name = "questionnaire_id"),
            inverseJoinColumns = @JoinColumn(name = "question_id")
    )
    private Set<QuestionBankEntity> questions;
}

