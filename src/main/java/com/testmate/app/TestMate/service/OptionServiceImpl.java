package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.dto.OptionDto;
import com.testmate.app.TestMate.entity.OptionEntity;
import com.testmate.app.TestMate.repository.IOptionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OptionServiceImpl implements IOptionService {

    @Autowired
    private IOptionRepository repository;

    @Autowired
    private ModelMapper mapper;


    @Override
    public OptionEntity save(OptionEntity entity) {
//        return repository.save(mapper.map(entity, OptionEntity.class));
        return repository.save(entity);
    }

    @Override
    public List<OptionEntity> getOptionsByQuestionId(String questionId) {
        return repository.findAllByQuestionId(questionId).stream().toList();
    }
}
