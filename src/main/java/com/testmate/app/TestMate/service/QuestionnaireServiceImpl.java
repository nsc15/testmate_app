package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.entity.ExamEntity;
import com.testmate.app.TestMate.entity.QuestionBankEntity;
import com.testmate.app.TestMate.entity.QuestionnaireEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.repository.IExamRepository;
import com.testmate.app.TestMate.repository.IQuestionBankRepository;
import com.testmate.app.TestMate.repository.IQuestionnaireRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class QuestionnaireServiceImpl implements IQuestionnaireService {
    @Autowired
    private IQuestionnaireRepository repository;

    @Autowired
    private IQuestionBankRepository questionBankRepository;

    @Autowired
    private IExamRepository examRepository;
    @Autowired
    private ModelMapper mapper;

    @Transactional
    @Override
    public QuestionnaireEntity add(QuestionnaireEntity entity) {
        ExamEntity examEntity = examRepository.findById(entity.getExam().getId())
                .orElseThrow(() -> new NotFoundException("Exam not found"));

        entity.setExam(examEntity);

        // Fetch questions from the question bank based on the provided difficulty levels
        Set<QuestionBankEntity> minDiffQuestionnaires = new HashSet<>(questionBankRepository.findByDifficultyLevel(entity.getMinDifficulty()));
        Set<QuestionBankEntity> maxDiffQuestionnaires = new HashSet<>(questionBankRepository.findByDifficultyLevel(entity.getMaxDifficulty()));
        System.out.println("Size of minDiffQuestionnaires: " + minDiffQuestionnaires.size());
        System.out.println("Size of maxDiffQuestionnaires: " + maxDiffQuestionnaires.size());
        // List to Set
        Set<QuestionBankEntity> questionBankEntitySet = Stream.concat(minDiffQuestionnaires.stream(), maxDiffQuestionnaires.stream()).collect(Collectors.toSet());
        entity.setQuestions(questionBankEntitySet);

        return repository.save(entity);
    }

    @Override
    public List<QuestionnaireEntity> listAll() {
        return repository.findAll();
    }

    @Override
    public QuestionnaireEntity get(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Questionnaire details not found."));
    }

    @Override
    public String delete(String id) {
        repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Questionnaire details not found."));
        repository.deleteById(id);
        // Return a confirmation message
        return "Questionnaire with ID " + id + " has been successfully deleted";
    }
}
