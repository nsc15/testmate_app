package com.testmate.app.TestMate.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.repository.ILanguageRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testmate.app.TestMate.dto.LanguageDto;
import com.testmate.app.TestMate.entity.LanguageEntity;


@Service
public class LanguageServiceImpl implements ILanguageService {

    @Autowired
    private ILanguageRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public LanguageEntity save(LanguageDto dto) {
        var languageEntity = mapper.map(dto, LanguageEntity.class);
        return this.repository.save(languageEntity);
    }

    @Override
    public List<LanguageEntity> fetchLanguageList() {
        return this.repository.findAll().stream().toList();
    }

    @Override
    public LanguageEntity getLanguageById(String languageId) {
        return this.repository
                .findById(languageId)
                .orElseThrow(() -> new NotFoundException("Language details not found."));
    }
}
