package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.dto.QuestionResponseDto;
import com.testmate.app.TestMate.entity.QuestionBankEntity;

import java.util.List;

public interface IQuestionBankService {
    public QuestionBankEntity add(QuestionResponseDto dto);

    public QuestionBankEntity update(QuestionResponseDto dto);

    public String delete(String id);

    public QuestionBankEntity get(String id);

    public List<QuestionBankEntity> listAll();

}
