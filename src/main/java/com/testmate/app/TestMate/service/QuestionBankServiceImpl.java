package com.testmate.app.TestMate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.testmate.app.TestMate.dto.OptionDto;
import com.testmate.app.TestMate.entity.OptionEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testmate.app.TestMate.dto.QuestionResponseDto;
import com.testmate.app.TestMate.entity.QuestionBankEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.repository.IQuestionBankRepository;
import org.springframework.transaction.annotation.Transactional;


@Service
public class QuestionBankServiceImpl implements IQuestionBankService {

    @Autowired
    private IQuestionBankRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private IOptionService optionService;

    @Override
    @Transactional
    public QuestionBankEntity add(QuestionResponseDto dto) {

        // Map DTO to entity
        QuestionBankEntity questionEntity = mapper.map(dto, QuestionBankEntity.class);

        // Set question for each option and save options
        List<OptionEntity> options = dto.getOptions();
        List<OptionEntity> optionEntitiesList = new ArrayList<>();
        for (OptionEntity optionEntity : options) {
            optionEntity.setQuestion(questionEntity);
            var savedOptionEntity = optionService.save(optionEntity);
            optionEntitiesList.add(savedOptionEntity);
        }
        questionEntity.setOptions(optionEntitiesList);

        return repository.save(questionEntity);
    }

    @Override
    public QuestionBankEntity update(QuestionResponseDto dto) {
        var questionBank = repository.findById(dto.getId());

        if (questionBank.isEmpty())
            throw new NotFoundException("Question not found.");
        return repository.save(mapper.map(dto, QuestionBankEntity.class));
    }

    @Override
    public String delete(String id) {
        // Check if the language exists
        Optional<QuestionBankEntity> languageOptional = repository.findById(id);
        if (languageOptional.isPresent()) {
            repository.deleteById(id);
            return "Deleted successfully";
        } else {
            throw new NotFoundException("Question not found.");
        }
    }

    @Override
    public QuestionBankEntity get(String id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundException("Question not found."));
    }

    @Override
    public List<QuestionBankEntity> listAll() {
        return repository.findAll();
    }
}
