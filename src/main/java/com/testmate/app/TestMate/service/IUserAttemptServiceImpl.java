package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.entity.UserAttemptEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.repository.IExamRepository;
import com.testmate.app.TestMate.repository.ILanguageRepository;
import com.testmate.app.TestMate.repository.IUserAttemptRepository;
import com.testmate.app.TestMate.repository.IUserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IUserAttemptServiceImpl implements IUserAttemptService {

    @Autowired
    private IUserAttemptRepository repository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IExamRepository examRepository;

    @Autowired
    private ILanguageRepository languageRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public UserAttemptEntity generate(UserAttemptEntity entity) {
        // TODO: need to process data before saving it to db.
        return repository.save(entity);
    }

    @Override
    public List<UserAttemptEntity> fetchByUserId(String userId) {
        userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException(String.format("User ID %s details not found", userId)));
        return repository.findByUserId(userId);
    }

    @Override
    public List<UserAttemptEntity> fetchByExamId(String examId) {
        examRepository.findById(examId)
                .orElseThrow(() -> new NotFoundException(String.format("Exam ID %s details not found", examId)));
        return repository.findByExamId(examId);
    }
}
