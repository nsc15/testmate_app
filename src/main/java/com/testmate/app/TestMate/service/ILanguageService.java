package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.dto.LanguageDto;
import com.testmate.app.TestMate.entity.LanguageEntity;

import java.util.List;
import java.util.Optional;

public interface ILanguageService {

    public LanguageEntity save(LanguageDto dto);

    public List<LanguageEntity> fetchLanguageList();

    public LanguageEntity getLanguageById(String languageId);
}
