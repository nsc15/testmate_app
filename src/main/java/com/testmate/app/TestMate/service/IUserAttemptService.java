package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.entity.UserAttemptEntity;

import java.util.List;

public interface IUserAttemptService {
    public UserAttemptEntity generate(UserAttemptEntity entity);

    public List<UserAttemptEntity> fetchByUserId(String userId);

    public List<UserAttemptEntity> fetchByExamId(String examId);

}
