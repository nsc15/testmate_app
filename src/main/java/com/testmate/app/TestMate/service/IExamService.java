package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.entity.ExamEntity;

import java.util.List;

public interface IExamService {
    public ExamEntity add(ExamEntity entity);

    public List<ExamEntity> listAll();

    public ExamEntity update(ExamEntity entity);

    public String delete(String id);

    public ExamEntity get(String id);
}
