package com.testmate.app.TestMate.service;


import com.testmate.app.TestMate.entity.UserEntity;

import java.util.List;

public interface IUserService {

    public UserEntity createUser(UserEntity entity);

    public UserEntity getUser(String userId);

    public String delete(String userId);

    public List<UserEntity> listAll();
}
