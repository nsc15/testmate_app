package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.entity.UserEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.repository.IUserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IUserServiceImpl implements IUserService {
    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public UserEntity createUser(UserEntity entity) {
        // TODO: encrypt the user password and store it in the db
        return userRepository.save(entity);
    }

    @Override
    public UserEntity getUser(String userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User not found"));
    }

    @Override
    public String delete(String userId) {
        userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User not found."));
        userRepository.deleteById(userId);
        // Return a confirmation message
        return "User with ID " + userId + " has been successfully deleted";
    }

    @Override
    public List<UserEntity> listAll() {
        return userRepository.findAll();
    }
}
