package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.entity.QuestionnaireEntity;

import java.util.List;

public interface IQuestionnaireService {

    public QuestionnaireEntity add(QuestionnaireEntity entity);

    public List<QuestionnaireEntity> listAll();

    public QuestionnaireEntity get(String id);

    public String delete(String id);
}
