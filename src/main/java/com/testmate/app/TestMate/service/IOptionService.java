package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.dto.OptionDto;
import com.testmate.app.TestMate.entity.OptionEntity;

import java.util.List;

public interface IOptionService {
    public OptionEntity save(OptionEntity entity);

    public List<OptionEntity> getOptionsByQuestionId(String questionId);
}
