package com.testmate.app.TestMate.service;

import com.testmate.app.TestMate.dto.ExamDto;
import com.testmate.app.TestMate.entity.ExamEntity;
import com.testmate.app.TestMate.exception.NotFoundException;
import com.testmate.app.TestMate.repository.IExamRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamServiceImpl implements IExamService {
    @Autowired
    private IExamRepository examRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public ExamEntity add(ExamEntity entity) {
        return examRepository.save(entity);
    }

    @Override
    public List<ExamEntity> listAll() {
        return examRepository.findAll();
    }

    @Override
    public ExamEntity update(ExamEntity entity) {
        var examEntity = examRepository
                .findById(entity.getId())
                .orElseThrow(() -> new NotFoundException("Exam details not found."));

        // Update the existing entity with the new values

        examEntity.setName(entity.getName());
        examEntity.setPassingScore(entity.getPassingScore());
        examEntity.setDurationInMinutes(entity.getDurationInMinutes());

        // Save the updated entity
        examRepository.save(entity);
        return examEntity;
    }

    @Override
    public String delete(String id) {
        examRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Exam details not found."));

        // Delete the entity
        examRepository.deleteById(id);

        // Return a confirmation message
        return "Exam with ID " + id + " has been successfully deleted";
    }

    @Override
    public ExamEntity get(String id) {

        return examRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Exam details not found."));
    }
}
